
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;

import com.nykredit.kundeservice.data.CTIRConnection;
import com.nykredit.kundeservice.swing.NFrame;


public class Main extends NFrame {

	private static final long serialVersionUID = 1L;
	private static Point point = new Point();
	public static String program = "Toolbox v3.3";
	public static ArrayList<Files> filesInFolder = new ArrayList<Files>();
	public static boolean newFile = false;
	public static void main(String[] args) {
		getFiles();

		Comparator comparator = new Comparator<Files>() {
			public int compare(Files c1, Files c2) {
				return c1.getName().compareTo(c2.getName()); // use your logic
			}
		};

		Collections.sort(filesInFolder,comparator);
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (Exception e) {
		}

		new Main();
	}

	public Main() {
		super(false);
		Thread spy = new Spy();
		spy.start();
		this.setUndecorated(true);
		initialize();
	}

	private void initialize(){
		this.setTitle(program);
		this.setContentPane(getMainPaneHorizontal());
		this.pack();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width / 2 - (this.getWidth() / 2), 0);
		this.setAlwaysOnTop(true);
		this.setVisible(true);		
		this.addMouseListener(new moveWindowMouseListener());
		this.addMouseMotionListener(new moveWindowMouseMotionListener());

	}

	public JPanel getMainPaneHorizontal() {
		JPanel p = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(2, 2, 2, 0);
		p.add(getExitButton(), c);
		c.insets = new Insets(2, 0, 2, 2);
		p.add(getMinimizeButton(), c);
		c.insets = new Insets(0, 0, 0, 2);

		p.add(getNameTag(), c);
		c.insets = new Insets(0, 0, 0, 0);

		for (Files f : filesInFolder) {
			String name = f.getName().substring(0,f.getName().indexOf("."));
			//System.out.println(name);
			String firstLetter = name.substring(0,1).toUpperCase();
			if (!name.equalsIgnoreCase("Toolbox") && ! name.equalsIgnoreCase("ToolBoxFinal")) {		
				p.add(getLinkButton(firstLetter +name.substring(1),f.getLocation()+"\\"+f.getName()),c);
			}


		}
		/*	p.add(getLinkButton("Mødeanalyse", "S:/S0KKCSTAT/Toolbox/Mødetidsanalyse.jar"),c);
		p.add(getLinkButton("Kviktid afv.",	"S:/S0KKCSTAT/Toolbox/kvi_afvig.jar"), c);
		p.add(getLinkButton("Email henv.","S:/S0KKCSTAT/Toolbox/Mail henvendelser.jar"), c);
		p.add(getLinkButton("Henvendelser", "http://nkm18f53:8080/henvendelser.jnlp"),c);
		p.add(getLinkButton("Statmaster", "S:/S0KKCSTAT/Toolbox/statmaster.jar"),c);
		p.add(getLinkButton("KPI-stat", "S:/S0KKCSTAT/Toolbox/kpistat.jar"), c);
		p.add(getLinkButton("Sygekalender","http://nkm18f53:8080/sygekalender.jnlp"), c);
		p.add(getLinkButton("Potlive","S:/S0KKCSTAT/Toolbox/potlive.jar"),c);
		 */
		return p;
	}

	public JPanel getMainPaneVertical() {
		JPanel p = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(2, 2, 2, 0);
		p.add(getExitButton(), c);
		c.insets = new Insets(2, 0, 2, 2);
		p.add(getMinimizeButton(), c);
		c.insets = new Insets(0, 0, 0, 2);
		p.add(getNameTag(), c);
		c.insets = new Insets(0, 0, 0, 0);
		c.gridy = 1;
		int i = 1;
		for (Files f : filesInFolder) {
			String name = f.getName().substring(0,f.getName().indexOf("."));
			if (!name.equalsIgnoreCase("Toolbox") && ! name.equalsIgnoreCase("ToolBoxFinal")) {
				p.add(getLinkButton(name,f.getLocation()+"\\"+f.getName()),c);
				c.gridy= i++;		
			}

		}

		/*	p.add(getLinkButton("Ad-hoc stat", "S:/S0KKCSTAT/Toolbox/AdhocStat.jar"),c);
		c.gridy = 2;
		p.add(getLinkButton("Mødetidsanalyse ", "S:/S0KKCSTAT/Toolbox/Mødetidsanalyse.jar"),c);
		c.gridy = 3;
		p.add(getLinkButton("Kviktid afv.",	"S:/S0KKCSTAT/Toolbox/kvi_afvig.jar"), c);
		c.gridy = 4;
		p.add(getLinkButton("Email henv.","S:/S0KKCSTAT/Toolbox/Mail henvendelser.jar"), c);
		c.gridy = 5;
		p.add(getLinkButton("Email stat", "S:/S0KKCSTAT/Toolbox/emailstat.jar"),c);
		c.gridy = 6;
		p.add(getLinkButton("Pause stat", "S:/S0KKCSTAT/Toolbox/Pause stat.jar"),c);
		c.gridy = 7;
		p.add(getLinkButton("Statmaster", "S:/S0KKCSTAT/Toolbox/statmaster.jar"),c);
		c.gridy = 8;
		p.add(getLinkButton("KPI-stat", "S:/S0KKCSTAT/Toolbox/kpistat.jar"), c);
		c.gridy = 9;
		p.add(getLinkButton("Sygekalender","S:/S0KKCSTAT/Toolbox/Sygekalender.xlsm"), c);
		 */
		return p;
	}
	private JLabel getNameTag() {
		final JLabel l = new JLabel(program, JLabel.CENTER);
		l.addMouseListener(new moveWindowMouseListener());
		l.addMouseMotionListener(new moveWindowMouseMotionListener());
		return l;
	}

	private JLabel getExitButton() {
		final JLabel b = new JLabel("X", JLabel.CENTER);
		b.setToolTipText("Luk programmet");
		b.setFont(new Font("Verdana", Font.BOLD, 12));
		final Color bgColor = b.getBackground();
		b.setPreferredSize(new Dimension(15, 15));
		b.setOpaque(true);
		b.addMouseListener(new MouseListener() {
			public void mouseClicked(MouseEvent e) {
				System.exit(0);
			}
			public void mousePressed(MouseEvent e) {
				b.setBackground(Color.orange);
				b.setBorder(BorderFactory.createLineBorder(Color.black));
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
				b.setBackground(Color.yellow);
				b.setBorder(BorderFactory.createLineBorder(Color.orange));
			}
			public void mouseExited(MouseEvent e) {
				b.setBackground(bgColor);
				b.setBorder(null);
			}
		});
		return b;
	}

	private JLabel getMinimizeButton() {
		final JLabel b = new JLabel("_", JLabel.CENTER);
		b.setToolTipText("Minimér vinduet");
		b.setFont(new Font("Verdana", Font.BOLD, 12));
		final Color bgColor = b.getBackground();
		b.setPreferredSize(new Dimension(15, 15));
		b.setOpaque(true);
		b.addMouseListener(new MouseListener() {
			public void mouseClicked(MouseEvent e) {
				Main.this.setState ( JFrame.ICONIFIED );
			}
			public void mousePressed(MouseEvent e) {
				b.setBackground(Color.orange);
				b.setBorder(BorderFactory.createLineBorder(Color.black));
			}
			public void mouseReleased(MouseEvent e) {
			}
			public void mouseEntered(MouseEvent e) {
				b.setBackground(Color.yellow);
				b.setBorder(BorderFactory.createLineBorder(Color.orange));
			}
			public void mouseExited(MouseEvent e) {
				b.setBackground(bgColor);
				b.setBorder(null);
			}
		});
		return b;
	}

	private JButton getLinkButton(String text, final String link) {
		JButton b = new JButton(text);
		System.out.println(text);
		b.setToolTipText("Åben "+text);
		b.setPreferredSize(new Dimension(90, 20));
		b.setMargin(new Insets(0, 0, 0, 0));
		System.out.println(link);
		b.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if(link.contains("http")){
						Desktop.getDesktop().browse(new URI(link));
					}else{
						Desktop.getDesktop().open(new File(link));
					}
				} catch (Exception e1) {
					System.out.println(e1);
				}
			}
		});
		return b;
	}
	class Spy extends Thread {
		private CTIRConnection oracle = new CTIRConnection();
		public void run() {
			Thread cur=Thread.currentThread();
			cur.setPriority(Thread.MIN_PRIORITY);
			try {
				oracle.Connect();
				oracle.SetupSpy("Toolbox 3.2");
				while (true) {
					Thread.sleep(1000);
					oracle.SS_spy();
				}
			} catch (Exception e) {System.out.println(e);}
		}	
	}
	class moveWindowMouseListener implements MouseListener{
		public void mouseClicked(MouseEvent e) {}
		public void mousePressed(MouseEvent e) {
			point.x = e.getX();
			point.y = e.getY();
		}
		public void mouseReleased(MouseEvent e) {}
		public void mouseEntered(MouseEvent e) {}
		public void mouseExited(MouseEvent e) {}
	}
	class moveWindowMouseMotionListener implements MouseMotionListener{

		public void mouseDragged(MouseEvent e) {
			Point p = Main.this.getLocation();
			int x = p.x + e.getX() - point.x;
			int y = p.y + e.getY() - point.y;
			if (x<0)
				x=0;
			if (y<0)
				y=0;
			Main.this.setLocation(x,y);
		}
		public void mouseMoved(MouseEvent e) {}
	}
	public static void getFiles(){
		ArrayList<String> myFileChoice = new ArrayList<String>();
		String line;
		try {
			BufferedReader br = new BufferedReader(new FileReader(new File("S:\\S0KKCSTAT\\Toolbox\\ToolBox settings\\programs.txt")));
			while ((line = br.readLine()) != null) {
				// process the line.
				myFileChoice.add(line);
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		File dir = new File("//NKM18F53/kundeservice");
		File[] matches = dir.listFiles(new FilenameFilter()
		{
			public boolean accept(File dir, String name)
			{
				return name.endsWith(".jar");
			}
		});
		for (File file : matches) {
				Files paths = new Files(dir.getPath(), file.getName());
				for (String string : myFileChoice) {
					if (string.equalsIgnoreCase(file.getName())) {
						filesInFolder.add(paths);					
					}
				}
		}

	}

}
