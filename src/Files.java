public class Files {

	private String path;
	private String name;
	
	public Files(String path, String name){
		this.path = path;
		this.name = name;
	}
	public String getLocation(){
		return path;
	}
	public String getName(){
		return name;
	}
	public String toString(){
		return name;
	}
	
}
